<?php

namespace Application\Bundle\TestBundle\Controller;

use Application\Bundle\TestBundle\Entity\AccessToken;
use Application\Bundle\TestBundle\Entity\Application;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    /**
     * @param Application $app
     *
     * @return array
     *
     * @ParamConverter("app", class="ApplicationTestBundle:Application")
     *
     * @Get("/access/token/{token}", options={"mapping": {"token": "token"}} )
     * @View()
     */
    public function indexAction(Application $app)
    {
        $now = new \DateTime();
        $expires = date('Y-m-d H:i:s', strtotime('+5 minutes', strtotime($now->format('Y-m-d H:i:s'))));

        $newAccessToken = new AccessToken();
        $newAccessToken->setAppId($app->getId());
        $newAccessToken->setToken(md5(rand(1,10000)));
        $newAccessToken->setExpires($expires);
        $em = $this->getDoctrine()->getManager();

        $em->persist($newAccessToken);
        $em->flush();

        return new JsonResponse(['token' => $newAccessToken->getToken()]);
    }

    /**
     * @param AccessToken $accessToken
     *
     * @return array
     *
     * @ParamConverter("accessToken", class="ApplicationTestBundle:AccessToken")
     *
     * @Get("/secret/{token}", options={"mapping": {"token": "token"}} )
     * @View()
     */
    public function secretAction(AccessToken $accessToken)
    {
        $now = date('Y-m-d H:i:s');
        if($accessToken->getExpires() > $now || $accessToken->getExpires() == $now) {
            return new JsonResponse(['expired_token']);
        }

        $em = $this->getDoctrine()->getManager();
        $secret = $em->getRepository('ApplicationTestBundle:Secret')->findOneBy(['id' =>1]);

        return new JsonResponse(['secret' => $secret->getSecret()]);
    }
}
