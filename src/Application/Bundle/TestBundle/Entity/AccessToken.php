<?php

namespace Application\Bundle\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccessToken
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Application\Bundle\TestBundle\Entity\AccessTokenRepository")
 */
class AccessToken
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * @var string
     *
     * @ORM\Column(name="expires", type="string", length=255)
     */
    private $expires;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Application")
     * @ORM\JoinColumn(name="app_id", referencedColumnName="id", nullable=false)
     * @ORM\Column(name="app_id", type="integer")
     */
    private $appId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return AccessToken
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set expires
     *
     * @param string $expires
     * @return AccessToken
     */
    public function setExpires($expires)
    {
        $this->expires = $expires;

        return $this;
    }

    /**
     * Get expires
     *
     * @return string 
     */
    public function getExpires()
    {
        return $this->expires;
    }

    /**
     * Set appId
     *
     * @param integer $appId
     * @return AccessToken
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * Get appId
     *
     * @return integer 
     */
    public function getAppId()
    {
        return $this->appId;
    }
}
