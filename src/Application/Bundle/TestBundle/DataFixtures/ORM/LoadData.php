<?php
namespace Application\Bundle\TestBundle\DataFixtures\ORM;

use Application\Bundle\TestBundle\Entity\Secret;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Bundle\TestBundle\Entity\Application;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $token = 'hf92hf92389238hf32jf9e9w928f223f';

        $app = new Application();
        $app->setToken('hf92hf92389238hf32jf9e9w928f223f');
        $app->setName('Test');

        $manager->persist($app);
        $manager->flush();

        $secret = new Secret();
        $secret->setSecret('QaquOZ0HyiYBNsbb+y7HAucbw4MKW23CmMHfnAO9nPg1+G0dT9gvoabofDVQzQnXlFv0HhKDq2u5/eb0DyzL8GN7QNijjHnh270oAA8B/Y5FpmjlIo2/sQRYIV51Wwu7vJmnP4d52x6x4b7ucSVSYhbAM22VZ54EG+fJRSClg9E=|vzmr/ZzvmZOk4bEEQPjxcc/RajHalVtaWEgPURxCZc0=');

        $manager->persist($secret);
        $manager->flush();
    }
}